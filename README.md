# Basic Steganography Image

> Bài thực tập cơ sở: TÌM HIỂU VỀ KĨ THUẬT ẨN – GIẤU TIN TRONG HÌNH ẢNH LSB STEGANOGRAPHY VÀ XÂY DỰNG ỨNG DỤNG.

## Giấu tin:

![Giấu tin](https://gitlab.com/c2at3/basic-steganography-image/-/raw/main/Report%20&%20Tools/1.png)
## Kết quả:

![Kết quả giấu tin](https://gitlab.com/c2at3/basic-steganography-image/-/raw/main/Report%20&%20Tools/2.png)

## Giải mã:

![Giải mã](https://gitlab.com/c2at3/basic-steganography-image/-/raw/main/Report%20&%20Tools/3.png)
